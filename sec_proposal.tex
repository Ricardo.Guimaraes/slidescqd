\section{Continuous Query Decomposition}

\begin{frame}{General form EPFO query} 

    \begin{align*}
        Q(A) \coloneqq ?A : \exists V_1,\dots,V_m.(e_1^1 \land \dots \land e_{n_1}^1) \lor \dots \lor (e_1^d \land \dots \land e_{n_d}^d),
    \end{align*}

    where 
    \begin{align*}
        e^j_i = 
        \begin{cases}
            r(c, V) &\text{with } V \in \{A, V_1, \dots V_m\}, c \in \entspace, r \in \relspace; \text{ or}\\
            r(V, V') &\text{with } V, V' \in \{A, V_1, \dots V_m\}, V \neq V', r \in \relspace
        \end{cases}
    \end{align*}
\end{frame}

\begin{frame}{Continuous Reformulation}
    \begin{align*}
        \argmax_{A, V_1, \dots V_m \in \entspace} (\embed{e}_1^1 \luand \dots \luand \embed{e}_{n_1}^1) \luor \dots \luor (\embed{e}_1^d \luand \dots \luand \embed{e}_{n_d}^d),
    \end{align*}

    where 
    \begin{align*}
        \embed{e}^j_i = 
        \begin{cases}
            \phi(\embed{c}, \embed{r}, \embed{V}) &\text{with } V \in \{A, V_1, \dots V_m\}, c \in \entspace, r \in \relspace; \text{ or}\\
            \phi(\embed{V}, \embed{r}, \embed{V'}) &\text{with } V, V' \in \{A, V_1, \dots V_m\}, V \neq V', r \in \relspace
        \end{cases}
    \end{align*}

    % TODO: annotate \phi, \luand, and \lour with tikzannotate equation
\end{frame}

\begin{frame}{Fuzzy T-norms and T-conorms}
    \begin{table}[]
        \centering
        \begin{tabular}{@{}lll@{}}
            \toprule
            Fuzzy Logic & t-norm (\(\luand\))    & t-conorm (\(\luor\))  \\ \midrule
            Gödel       & \(\min(a, b)\)         & \(\max(a, b)\)        \\
            Product     & \(a \cdot b\)          & \(a + b - a \cdot b\) \\
            Łukasiewicz & \(\max(0, a + b - 1)\) & \(\min(a + b, 1)\)    \\ \bottomrule
        \end{tabular}
    \end{table}
\end{frame}

\begin{frame}{Example: Computing Query Answer Score}

    ``In which genres are the books written by Stephen King?''
        \begin{align*}
            ?G : \exists B.wrote(\mathsf{King}, B) \land hasGenre(B, G)\\
        \end{align*}

        \begin{itemize}
            \item Let us assign ``It'' to \(B\) and ``Horror'' to \(G\)
            \item Use Gödel t-norm and t-conorm
            \item An arbitrary link predictor \(\phi\)
        \end{itemize}


        \begin{align*}
            &\min(\phi(\embed{King}, \embed{wrote}, \embed{It}), \phi(\embed{It}, \embed{hasGenre}, \embed{Horror})) =\\
            &\min(0.4375, 1) = 0.4375
        \end{align*}
    
\end{frame}

\begin{frame}{Continuous Optimisation (CQD-CO)}
    Find the vectors directly

    \begin{align*}
        \argmax_{\textcolor{oiblue}{\embed{A}, \embed{V_1}, \dots \embed{V_m} \in \symbb{R}^k}} (\embed{e}_1^1 \luand \dots \luand \embed{e}_{n_1}^1) \luor \dots \luor (\embed{e}_1^d \luand \dots \luand \embed{e}_{n_d}^d),
    \end{align*}

    where 
    \begin{align*}
        \embed{e}^j_i = 
        \begin{cases}
            \phi(\embed{c}, \embed{r}, \embed{V}) &\text{with } V \in \{A, V_1, \dots V_m\}, c \in \entspace, r \in \relspace; \text{ or}\\
            \phi(\embed{V}, \embed{r}, \embed{V'}) &\text{with } V, V' \in \{A, V_1, \dots V_m\}, V \neq V', r \in \relspace
        \end{cases}
    \end{align*}

    \begin{itemize}[itemsep=1pt]
        \item Use gradient-based optimisation to find the best options for \(\embed{A}, \embed{V_1}, \dots \embed{V_m}\) % (e.g. Adam~\cite{Kingma2015})
        \item Replace \(\embed{A}\) with every possible \(\embed{c}\) for \(c \in \entspace\)
        \item Keep the best answers (w.r.t.\ query score)
    \end{itemize}
\end{frame}

\begin{frame}{Combinatorial Optimisation (CQD-Beam)}


    \begin{itemize}
        \item Explore the space of all possible entity-to-variable assignments
        \item Use the dependency graph from anchors to the answer variable 
        \item Take the top ``n'' predictions at each step
    \end{itemize}

\end{frame}

\begin{frame}{CQD-Beam: Example}
    ``In which genres are the books written by Stephen King?''
        \begin{align*}
            ?G : \exists B.wrote(\mathsf{King}, B) \land hasGenre(B, G)\\
        \end{align*}

        \centering\input{cqgraph_anim.tex}

        \begin{columns}[t]
            \column{0.45\textwidth}
            \begin{visibleenv}<2->%
                \textcolor<3>{oibluishgreen}{The Dark Tower}\\ 
                \textcolor<4>{oibluishgreen}{The Talisman}\\
                \textcolor<5>{oibluishgreen}{It}
            \end{visibleenv}
            \column{0.45\textwidth}
            \begin{onlyenv}<3>%
                Fantasy\\ 
                Western\\
                SciFi
            \end{onlyenv}%
            \begin{onlyenv}<4>%
                Fantasy\\
                Horror\\
                SciFi
            \end{onlyenv}%
            \begin{onlyenv}<5>%
                Horror\\ 
                Fantasy\\
                Thriller
            \end{onlyenv}%
        \end{columns}
\end{frame}

\begin{frame}{CQD-Beam: Example (continued)}

    Rank all possible fillers for \(G\) according to the reformulated EPFO score:
    \begin{itemize}[itemsep=1pt]
        \item[1.] Horror
        \item[2.] Fantasy
        \item[3.] SciFi
        \item[4.] Thriller
        \item[5.] Western
    \end{itemize}
   
    Decided a threshold on the ranking and return the best alternatives
\end{frame}

